﻿using Core.Data.Common;
using Library.Data.Interface;
using Library.Data.Repositories;

namespace Console
{
    public class ConfiguraRepos
    {
        public static IServiceCollection ConfiguraRepositories(IServiceCollection services)
        {
            services.AddScoped<IAlunoRepository, AlunoRepository>();
            services.AddScoped<IProfessorRepository, ProfessorRepository>();
            services.AddScoped<IDisciplinaRepository, DisciplinaRepository>();
            services.AddScoped<ITccRepository, TccRepository>();
            services.AddScoped<IArquivoRepository, ArquivoRepository>();
            services.AddScoped<IMatriculaRepository, MatriculaRepository>();
            services.AddScoped<IHomeRepository, HomeRepository>();
            services.AddScoped<IBancaRepository, BancaRepository>();
            services.AddScoped<IAlunoTccRepository, AlunoTccRepository>();
            services.AddScoped<IProfessorTccRepository, ProfessorTccRepository>();
            services.AddScoped<IProfessorBancaRepository, ProfessorBancaRepository>();
            services.AddScoped(typeof(ICommonRepository<>), typeof(CommonRepository<>));
            return services;
        }
    }
}
