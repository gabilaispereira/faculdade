﻿using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class ProfessorController : Controller
    {
        private readonly IProfessorRepository _professorRepository;
        public ProfessorController(IProfessorRepository professorRepository)
        {
            _professorRepository = professorRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_professorRepository.GetAll(), loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Professor();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _professorRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _professorRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _professorRepository.Update(objeto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _professorRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }
    }
}
