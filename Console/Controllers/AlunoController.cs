﻿using Console.Extensions;
using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class AlunoController : Controller
    {
        private readonly IAlunoRepository _alunoRepository;
        private readonly IMatriculaRepository _matriculaRepository;
        public AlunoController(IAlunoRepository alunoRepository, IMatriculaRepository matriculaRepository) 
        {
            _alunoRepository = alunoRepository;
            _matriculaRepository = matriculaRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_alunoRepository.GetAll(), loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Aluno();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _alunoRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _alunoRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _alunoRepository.Update(objeto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _alunoRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

        [HttpGet]
        public object GetMatricula(string cpf, DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_matriculaRepository.GetAll().Where(w => w.CPFAluno == cpf), loadOptions);
        }

        [HttpPost]
        public IActionResult PostMatricula(string values)
        {
            var objeto = new Matricula();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _matriculaRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult DeleteMatricula(string key)
        {
            try
            {
                _matriculaRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

    }
}
