﻿using Console.Extensions;
using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class ArquivoController : Controller
    {
        private readonly IArquivoRepository _arquivoRepository;
        public ArquivoController(IArquivoRepository arquivoRepository) 
        {
            _arquivoRepository = arquivoRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_arquivoRepository.GetAll(), loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Arquivo();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _arquivoRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _arquivoRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _arquivoRepository.Update(objeto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _arquivoRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

    }
}
