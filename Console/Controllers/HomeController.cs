﻿using Microsoft.AspNetCore.Mvc;
using Console.Extensions;
using Core.Data.Common;
using Library.ViewModels;
using Library.Data.Interface;

namespace Console.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeRepository _homeRepository;
        public HomeController(IHomeRepository homeRepository) {
            _homeRepository = homeRepository;
        }

        public IActionResult Index()
        {
            var vm = _homeRepository.GetDashboard();
            return View(vm);
        }

        [HttpGet]
        public object GetMenu(string menuAtual)
        {
            var menu = Menu.GetByTipo(Url, menuAtual);

            return menu;
        }

    }
}
