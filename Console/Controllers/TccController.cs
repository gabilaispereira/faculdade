﻿using Console.Extensions;
using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class TccController : Controller
    {
        private readonly ITccRepository _tccRepository;
        private readonly IAlunoTccRepository _alunoTccRepository;
        private readonly IProfessorTccRepository _professorTccRepository;
        private readonly IArquivoRepository _arquivoRepository;
        public TccController(ITccRepository tccRepository, IAlunoTccRepository alunoTccRepository, IArquivoRepository arquivoRepository, IProfessorTccRepository professorTccRepository) 
        {
            _tccRepository = tccRepository;
            _alunoTccRepository = alunoTccRepository;
            _arquivoRepository = arquivoRepository;
            _professorTccRepository = professorTccRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            var tccs = _tccRepository.GetAll();

            foreach (var tcc in tccs)
            {
                var arquivo = _arquivoRepository.GetByKey(tcc.CodigoArquivo.ToString());

                tcc.NomeArquivo = arquivo.NomeArquivo;
                tcc.Url = arquivo.Url;
            }

            return DataSourceLoader.Load(tccs, loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Tcc();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                var ultimoArquivo = _arquivoRepository.GetAll().OrderByDescending(o => o.Codigo).FirstOrDefault();

                var arquivo = new Arquivo
                {
                    Codigo = ultimoArquivo == null ? 0 : ultimoArquivo.Codigo + 1,
                    NomeArquivo = objeto.NomeArquivo,
                    Url = objeto.Url
                };

                _arquivoRepository.Insert(arquivo);

                objeto.CodigoArquivo = arquivo.Codigo;

                _tccRepository.Insert(objeto);


                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _tccRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _tccRepository.Update(objeto);

                var arquivo = _arquivoRepository.GetByKey(objeto.CodigoArquivo.ToString());

                arquivo.NomeArquivo = objeto.NomeArquivo;
                arquivo.Url = objeto.Url;

                _arquivoRepository.Update(arquivo);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _tccRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

        [HttpGet]
        public object GetAlunos(int codigo, DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_alunoTccRepository.GetAll().Where(w => w.CodigoTcc == codigo), loadOptions);
        }

        [HttpPost]
        public IActionResult PostAluno(string values)
        {
            var objeto = new AlunoTcc();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _alunoTccRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult DeleteAluno(string key)
        {
            try
            {
                _alunoTccRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

        [HttpGet]
        public object GetProfessor(int codigoTcc, DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_professorTccRepository.GetAll().Where(w => w.CodigoTcc == codigoTcc), loadOptions);
        }

        [HttpPost]
        public IActionResult PostProfessor(string values)
        {
            var objeto = new ProfessorTcc();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _professorTccRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult DeleteProfessor(string key)
        {
            try
            {
                _professorTccRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

    }
}
