﻿using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class DisciplinaController : Controller
    {
        private readonly IDisciplinaRepository _disciplinaRepository;
        public DisciplinaController(IDisciplinaRepository disciplinaRepository)
        {
            _disciplinaRepository = disciplinaRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_disciplinaRepository.GetAll(), loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Disciplina();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _disciplinaRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _disciplinaRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _disciplinaRepository.Update(objeto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _disciplinaRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }
    }
}
