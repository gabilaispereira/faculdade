﻿using Core.Data.Common;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Console.Controllers
{
    public class BancaController : Controller
    {
        private readonly IBancaRepository _bancaRepository;
        private readonly IProfessorBancaRepository _professorBancaRepository;
        private readonly IProfessorRepository _professorRepository;
        public BancaController(IBancaRepository bancaRepository, IProfessorBancaRepository professorBancaRepository, IProfessorRepository professorRepository)
        {
            _bancaRepository = bancaRepository;
            _professorBancaRepository = professorBancaRepository;
            _professorRepository = professorRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_bancaRepository.GetAll(), loadOptions);
        }

        [HttpPost]
        public IActionResult Post(string values)
        {
            var objeto = new Banca();
            JsonConvert.PopulateObject(values, objeto);
            try
            {
                _bancaRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(string Key, string values)
        {
            try
            {
                var objeto = _bancaRepository.GetByKey(Key);
                JsonConvert.PopulateObject(values, objeto);
                _bancaRepository.Update(objeto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao editar: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string key)
        {
            try
            {
                _bancaRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }

        [HttpGet]
        public object GetProfessor(int codigo, DataSourceLoadOptions loadOptions)
        {
            var profBancas = _professorBancaRepository.GetAll().Where(w => w.CodigoBanca == codigo);

            var retorno = new List<Professor>();

            foreach(var profBanca in profBancas)
            {
                var professor = _professorRepository.GetByKey(profBanca.CPFProfessor);

                retorno.Add(professor);
            }


            return DataSourceLoader.Load(retorno, loadOptions);
        }

        [HttpPost]
        public IActionResult AddProfessor(string values)
        {

            var objeto = new ProfessorBanca();
            JsonConvert.PopulateObject(values, objeto);

            try
            {
                _professorBancaRepository.Insert(objeto);
                return Ok(objeto);
            }

            catch (Exception e)
            {
                return BadRequest("Erro ao inserir: " + e.Message);
            }
        }

        [HttpDelete]
        public IActionResult DeleteProfessor(string key)
        {
            try
            {
                _professorBancaRepository.DeleteByKey(key);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Erro ao excluir: " + e.Message);
            }
        }
    }
}
