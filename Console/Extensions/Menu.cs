﻿using Microsoft.AspNetCore.Mvc;
using Core.ViewModels;

namespace Console.Extensions
{
    public static class Menu
    {
        private static List<MenuViewModel> ItensMenu(IUrlHelper url, string menuAtual)
        {
            var menu = new List<MenuViewModel>
            {
                //new MenuViewModel("Notificação", "bx bxs-bell-ring", url.Action("Index", "Notificacao")),
            };

            menu.ForEach(m => m.Selecionado = (m.Nome == menuAtual));

            return menu;
        }

        public static List<MenuViewModel> GetByTipo(IUrlHelper url, string menuAtual)
        {
            return ItensMenu(url, menuAtual).ToList();
        }
    }
}