﻿
using Fafabot.Core.Data.Interfaces;
using Fafabot.Core.Data.Repositories;
using Fafabot.Core.Entities.Common;
using Fafabot.Core.Services;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Fafabot.Console
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //String de conexão
            string Conexao = Configuration.GetConnectionString("FafabotConnection");

            services.AddDbContext<Core.Context.FafabotContext>(options =>
            {
                options.UseSqlServer(Conexao);
            });

            // Configurações do Identity
            services.AddIdentity<Usuario, IdentityRole<Guid>>()
                .AddEntityFrameworkStores<Core.Context.FafabotContext>()
                .AddDefaultTokenProviders();

            services.AddControllersWithViews().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = new PathString("/Login");
                options.LoginPath = new PathString("/Login");
            });

            services.AddSignalR();
            ConfiguraRepos.ConfiguraRepositories(services);

            services.AddTransient<IEmailSender, EmailSender>(i =>
                new EmailSender(
                    Configuration["EmailSender:Host"],
                    Configuration.GetValue<int>("EmailSender:Port"),
                    Configuration.GetValue<bool>("EmailSender:EnableSSL"),
                    Configuration["EmailSender:UserName"],
                    Configuration["EmailSender:Password"],
                    Configuration["EmailSender:Alias"]
                )
            );        

            
            services.AddScoped<IContatoRepository, ContatoRepository>();

            //CADASTROSHORASEXTRA:
            services.AddScoped<ICompraRepository, CompraRepository>();
            services.AddScoped<ICompraItemRepository, CompraItemRepository>();
        }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
