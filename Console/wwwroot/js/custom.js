﻿function sairSistema() {
    exibeLoadPanel('Desconectando...');
    window.location.href = "/Sair";
}

/* ================================================================= 
    função de salvar Layout, Limpar e Carregar
================================================================= */
function customLoadLayout() {
    var state = localStorage.getItem(this.storageKey);
    if (state) {
        state = JSON.parse(state);
    }
    return state;
}
function customSaveLayout(key, state) {
    localStorage.setItem(key, JSON.stringify(state));
}
function clearLayout(key) {
    localStorage.setItem(key, null);
}

/* ================================================================= 
    função que verifica se a variável está vazia
================================================================= */
function isNull(value) {
    return value === undefined || value === null || value === '';
}

function exibePopupTemplate(titulo, template, altura, largura, fullScreen = smallDevice(), showClose = false, nomePopup = null, visivel = true, closeOnOutsideClick = false) {
    if (isNull(nomePopup)) {

        escondeAviso();

        $("#popup-aviso-padrao").dxPopup({
            contentTemplate: $("#" + template),
            fullScreen: fullScreen,
            position: { my: 'center', at: 'center', of: window },
            showCloseButton: showClose,
            showTitle: !isNull(titulo),
            tabIndex: 0,
            title: titulo,
            titleTemplate: "title",
            visible: visivel,
            closeOnOutsideClick: closeOnOutsideClick,
            width: largura,
            height: altura,
        });
        $("#popup-aviso-padrao").dxPopup({
            position: { my: 'center', at: 'center', of: window },
        });
    }
    else {
        $("#" + nomePopup).dxPopup("instance").option("fullScreen", fullScreen);
        $("#" + nomePopup).dxPopup("instance").option("showCloseButton", showClose);
        $("#" + nomePopup).dxPopup("instance").option("showTitle", !isNull(titulo));
        $("#" + nomePopup).dxPopup("instance").option("title", titulo);
        $("#" + nomePopup).dxPopup("instance").option("width", largura);
        $("#" + nomePopup).dxPopup("instance").option("height", altura);
        $("#" + nomePopup).dxPopup("instance").option("visible", visivel);
        $("#" + nomePopup).dxPopup("instance").option("closeOnOutsideClick", closeOnOutsideClick);
        $("#" + nomePopup).dxPopup("instance").show();
    }
}

function exibePainelConfiguracao(titulo, template) {
    escondeAviso();

    $("#popup-aviso-padrao").dxPopup({
        animation: {
            show: {
                type: 'slide', duration: 400, from: { position: { my: 'left', at: 'right', of: window } }
            },
            hide: {
                type: 'slide', duration: 400, to: { position: { my: 'left', at: 'right', of: window } } 
            }
        },
        contentTemplate: $("#" + template),
        elementAttr: {class: "popup-configuration"},
        position: { my: 'right', at: 'right', of: window },
        showCloseButton: true,
        showTitle: true,
        title: titulo,
        tabIndex: 0,
        visible: true,
        width: "25%",
        height: "100vh",
        fullScreen: smallDevice()
    });


}

function exibeAvisoSimNao(titulo, msg, funcaoSim, largura = "300px", altura = "auto") {
    EscondeAvisoPersonalizado('popup-aviso-padrao-simnao');
    var corpo = $("<div id='corpo-popup-aviso-padrao-simnao' style='text-align:center'><div style='margin-bottom:10px'>" + msg + "</div> <div id='botao-corpo-popup-aviso-padrao-nao'></div><div id='botao-corpo-popup-aviso-padrao-sim'></div> </div>");
    $("#popup-aviso-padrao-simnao").dxPopup({
        contentTemplate: corpo,
        fullScreen: false,
        position: { my: 'center', at: 'center', of: window },
        showCloseButton: false,
        showTitle: !isNull(titulo),
        tabIndex: 0,
        title: titulo,
        titleTemplate: "title",
        visible: true,
        width: largura,
        height: altura
    });
    $("#botao-corpo-popup-aviso-padrao-nao").dxButton({
        text: "Não",
        onClick: function (e) {
            EscondeAvisoPersonalizado('popup-aviso-padrao-simnao');
        },
        stylingMode: 'text'
    }); $("#botao-corpo-popup-aviso-padrao-sim").dxButton({
        text: "Sim",
        stylingMode: 'text',
        type: 'default',
        onClick: function (e) {
            EscondeAvisoPersonalizado('popup-aviso-padrao-simnao');
            window[funcaoSim](e);
        }
    });
}

function escondeAviso() {
    if (!isNull($("#popup-aviso-padrao"))) {
        $("#popup-aviso-padrao").remove();
            $("body").append("<div id='popup-aviso-padrao'></div>");
    }
}
function exibeLoadPanel(posicao) {
    escondeLoadPanel();

    $("#load-panel-padrao").dxLoadPanel({
        position: isNull(posicao) ? { my: 'center', at: 'center', of: window } : posicao,
        visible: true,
        showIndicator: true,
        showPane: true,
        shading: false,
        closeOnOutsideClick: false
    });
    $("#load-panel-padrao").dxLoadPanel({
        position: isNull(posicao) ? { my: 'center', at: 'center', of: window } : posicao,
    });
}
function escondeLoadPanel() {
    if (!isNull($("#load-panel-padrao"))) {
        $("#load-panel-padrao").remove();
        $("body").append("<div id='load-panel-padrao'></div>");
    }
}

function notifica(mensagem, sucesso = true, tempo = 4000) {
    DevExpress.ui.notify(mensagem, sucesso ? "success" : "error", 4000);
}



function getTotalGrid(grid) {
    grid.option("dataSource")
        .store
        .totalCount()
        .done(function (data) {
            if (!isNull($("#totalCountInfo"))) {
                $("#totalCountInfo").text(data + " itens");
            }
        });
}
function smallDevice() {
    return document.documentElement.clientWidth <= 1200;
}
function bigDevice() {
    return !smallDevice();
}
function gridHeight() {
    if (smallDevice()) {
        return "calc(100vh - 111px)";
    }
    else {
        return "calc(100vh - 115px)";
    }
}

function gridHeightTab() {
    if (smallDevice()) {
        return "calc(95vh - 121px)";
    }
    else {
        return "calc(95vh - 132px)";
    }
}

/* =================================================================
    Funções de formatações brasileiras
================================================================= */
function formataMoeda(e) {
    if (isNull(e))
        return "";
    return e.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
}
function formataNumero(e) {
    if (isNull(e))
        return "";
    return e.toLocaleString('pt-BR');
}
function formataNumeroUmDecimal(e) {
    if (isNull(e))
        return "";
    return (Math.round(parseFloat(e) * 10) / 10).toFixed(1);
}
function formataDataLonga(e) {
    if (isNull(e))
        return "";
    return e.toLocaleString('pt-BR');
}
function formataDataCurta(e) {
    if (isNull(e))
        return "";
    return e.toLocaleString('pt-BR').substring(0, 10).split('-').reverse().join('/');
}

function formataPercentual(e) {
    return formataNumero(e * 100) + "%";
}

function exibeTooltip(target, texto, visivel = true) {
    if (!isNull($("#tooltip-padrao"))) {
        $("#tooltip-padrao").remove();
        $("body").append("<div id='tooltip-padrao'></div>");
    }
    $("#tooltip-padrao").dxTooltip({
        target: target,
        visible: visivel,
        hideEvent: "mouseleave",
        contentTemplate: texto
    });

    $("#tooltip-padrao").show();
}