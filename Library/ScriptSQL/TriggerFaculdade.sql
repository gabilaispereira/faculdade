CREATE TRIGGER att_professor_disciplina
ON Disciplina
FOR DELETE
AS
BEGIN
	UPDATE Professor SET CodigoDisciplina = NULL WHERE CodigoDisciplina = (Select top 1 Codigo from deleted);
END;