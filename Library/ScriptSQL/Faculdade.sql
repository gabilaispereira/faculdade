
CREATE TABLE [Aluno] (
    [Cpf] nvarchar(11) NOT NULL,
    [Ra] nvarchar(9) NOT NULL,
    [Nome] nvarchar(255) NOT NULL,
    [DataIngressao] datetime2 NOT NULL,
    [DataConclusao] datetime2 NULL,
    CONSTRAINT [PK_Aluno] PRIMARY KEY ([Cpf])
);

CREATE TABLE [Arquivo] (
    [Codigo] int NOT NULL IDENTITY,
    [NomeArquivo] nvarchar(255) NOT NULL,
    [Url] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Arquivo] PRIMARY KEY ([Codigo])
);

CREATE TABLE [Disciplina] (
    [Codigo] int NOT NULL IDENTITY,
    [Nome] nvarchar(255) NOT NULL,
    [CargaHoraria] int NULL,
    CONSTRAINT [PK_Disciplina] PRIMARY KEY ([Codigo])
);

CREATE TABLE [Matricula] (
    [Codigo] int NOT NULL IDENTITY,
    [CPFAluno] nvarchar(11) NOT NULL,
    [CodigoDisciplina] int NOT NULL,
    CONSTRAINT [PK_Matricula] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_Matricula_Aluno_CPFAluno] FOREIGN KEY ([CPFAluno]) REFERENCES [Aluno] ([Cpf]) ON DELETE CASCADE,
    CONSTRAINT [FK_Matricula_Disciplina_CodigoDisciplina] FOREIGN KEY ([CodigoDisciplina]) REFERENCES [Disciplina] ([Codigo]) ON DELETE CASCADE
);

CREATE TABLE [Professor] (
    [Cpf] nvarchar(11) NOT NULL,
    [Nome] nvarchar(255) NOT NULL,
    [Especializacao] nvarchar(255) NULL,
    [CodigoDisciplina] int NULL,
    CONSTRAINT [PK_Professor] PRIMARY KEY ([Cpf]),
    CONSTRAINT [FK_Professor_Disciplina_CodigoDisciplina] FOREIGN KEY ([CodigoDisciplina]) REFERENCES [Disciplina] ([Codigo])
);

CREATE TABLE [Tcc] (
    [Codigo] int NOT NULL IDENTITY,
    [Titulo] nvarchar(max) NOT NULL,
    [QuantidadePalavras] int NOT NULL,
    [CodigoArquivo] int NOT NULL,
    [CpfProfessor] nvarchar(11) NULL,
    CONSTRAINT [PK_Tcc] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_Tcc_Arquivo_CodigoArquivo] FOREIGN KEY ([CodigoArquivo]) REFERENCES [Arquivo] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [FK_Tcc_Professor_CpfProfessor] FOREIGN KEY ([CpfProfessor]) REFERENCES [Professor] ([Cpf])
);

CREATE TABLE [AlunoTcc] (
    [Codigo] int NOT NULL IDENTITY,
    [CPFAluno] nvarchar(11) NOT NULL,
    [CodigoTcc] int NOT NULL,
    CONSTRAINT [PK_AlunoTcc] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_AlunoTcc_Aluno_CPFAluno] FOREIGN KEY ([CPFAluno]) REFERENCES [Aluno] ([Cpf]) ON DELETE CASCADE,
    CONSTRAINT [FK_AlunoTcc_Tcc_CodigoTcc] FOREIGN KEY ([CodigoTcc]) REFERENCES [Tcc] ([Codigo]) ON DELETE CASCADE
);

CREATE TABLE [Banca] (
    [Codigo] int NOT NULL IDENTITY,
    [DataHoraBanca] datetime2 NOT NULL,
    [CodigoTcc] int NOT NULL,
    CONSTRAINT [PK_Banca] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_Banca_Tcc_CodigoTcc] FOREIGN KEY ([CodigoTcc]) REFERENCES [Tcc] ([Codigo]) ON DELETE CASCADE
);

CREATE TABLE [ProfessorTcc] (
    [Codigo] int NOT NULL IDENTITY,
    [CPFProfessor] nvarchar(11) NOT NULL,
    [CodigoTcc] int NOT NULL,
    CONSTRAINT [PK_ProfessorTcc] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_ProfessorTcc_Professor_CPFProfessor] FOREIGN KEY ([CPFProfessor]) REFERENCES [Professor] ([Cpf]) ON DELETE CASCADE,
    CONSTRAINT [FK_ProfessorTcc_Tcc_CodigoTcc] FOREIGN KEY ([CodigoTcc]) REFERENCES [Tcc] ([Codigo]) ON DELETE CASCADE
);

CREATE TABLE [ProfessorBanca] (
    [Codigo] int NOT NULL IDENTITY,
    [CPFProfessor] nvarchar(11) NOT NULL,
    [CodigoBanca] int NOT NULL,
    CONSTRAINT [PK_ProfessorBanca] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_ProfessorBanca_Banca_CodigoBanca] FOREIGN KEY ([CodigoBanca]) REFERENCES [Banca] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [FK_ProfessorBanca_Professor_CPFProfessor] FOREIGN KEY ([CPFProfessor]) REFERENCES [Professor] ([Cpf]) ON DELETE CASCADE
);

CREATE INDEX [IX_AlunoTcc_CodigoTcc] ON [AlunoTcc] ([CodigoTcc]);

CREATE INDEX [IX_AlunoTcc_CPFAluno] ON [AlunoTcc] ([CPFAluno]);

CREATE INDEX [IX_Banca_CodigoTcc] ON [Banca] ([CodigoTcc]);

CREATE INDEX [IX_Matricula_CodigoDisciplina] ON [Matricula] ([CodigoDisciplina]);

CREATE INDEX [IX_Matricula_CPFAluno] ON [Matricula] ([CPFAluno]);

CREATE INDEX [IX_Professor_CodigoDisciplina] ON [Professor] ([CodigoDisciplina]);

CREATE INDEX [IX_ProfessorBanca_CodigoBanca] ON [ProfessorBanca] ([CodigoBanca]);

CREATE INDEX [IX_ProfessorBanca_CPFProfessor] ON [ProfessorBanca] ([CPFProfessor]);

CREATE INDEX [IX_ProfessorTcc_CodigoTcc] ON [ProfessorTcc] ([CodigoTcc]);

CREATE INDEX [IX_ProfessorTcc_CPFProfessor] ON [ProfessorTcc] ([CPFProfessor]);

CREATE INDEX [IX_Tcc_CodigoArquivo] ON [Tcc] ([CodigoArquivo]);

CREATE INDEX [IX_Tcc_CpfProfessor] ON [Tcc] ([CpfProfessor]);