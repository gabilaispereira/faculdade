﻿using Core.Entities.Common;
using Library.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Core.Context
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options) { }

        #region -> DbSet

        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Disciplina> Disciplinas { get; set; }
        public DbSet<Matricula> Matriculas { get; set; }
        public DbSet<Tcc> Tccs { get; set; }
        public DbSet<Arquivo> Arquivos { get; set; }
        public DbSet<Banca> Bancas { get; set; }
        public DbSet<AlunoTcc> AlunoTccs { get; set; }
        public DbSet<ProfessorTcc> ProfessorTccs { get; set; }
        public DbSet<ProfessorBanca> ProfessorBancas { get; set; }

        #endregion


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region -> Configure
            base.OnModelCreating(modelBuilder);

            #endregion
        }


     
    }
}