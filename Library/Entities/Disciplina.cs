﻿using Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Entities
{
    [Table("Disciplina")]
    public class Disciplina : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [MaxLength(255)]
        public string Nome { get; set; }

        public int? CargaHoraria { get; set; }

    }
}
