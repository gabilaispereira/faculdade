﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("Aluno")]
    public class Aluno : CommonEntity
    {
        [Key]
        [MaxLength(11)]
        public string Cpf { get; set; }
        [MaxLength(9)]
        public string Ra { get; set; }
        [MaxLength(255)]
        public string Nome { get; set; }
        public DateTime DataIngressao { get; set; }
        public DateTime? DataConclusao { get; set; }
    }
}
