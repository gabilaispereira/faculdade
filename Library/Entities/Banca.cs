﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("Banca")]
    public class Banca : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }
        public DateTime DataHoraBanca { get; set; }
        [ForeignKey("Tcc")]
        public int CodigoTcc { get; set; }
        public virtual Tcc Tcc { get; set; }
    }
}
