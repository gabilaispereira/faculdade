﻿using Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Entities
{
    [Table("Matricula")]
    public class Matricula : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [ForeignKey("Aluno")]
        public string CPFAluno { get; set; }
        public virtual Aluno Aluno { get; set; }

        [ForeignKey("Disciplina")]
        public int CodigoDisciplina { get; set; }
        public virtual Disciplina Disciplina { get; set; }

    }
}
