﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("Professor")]
    public class Professor : CommonEntity
    {
        [Key]
        [MaxLength(11)]
        public string Cpf { get; set; }

        [MaxLength(255)]
        public string Nome { get; set; }

        [MaxLength(255)]
        public string? Especializacao { get; set; }

        [ForeignKey("Disciplina")]
        public int? CodigoDisciplina { get; set; }
        public virtual Disciplina Disciplina { get; set; }
    }
}
