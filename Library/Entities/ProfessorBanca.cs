﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("ProfessorBanca")]
    public class ProfessorBanca : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [ForeignKey("Professor")]
        public string CPFProfessor { get; set; }
        public virtual Professor Professor { get; set; }

        [ForeignKey("Banca")]
        public int CodigoBanca { get; set; }
        public virtual Banca Banca { get; set; }
    }
}
