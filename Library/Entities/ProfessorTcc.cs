﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("ProfessorTcc")]
    public class ProfessorTcc : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [ForeignKey("Professor")]
        public string CPFProfessor { get; set; }
        public virtual Professor Professor { get; set; }

        [ForeignKey("Tcc")]
        public int CodigoTcc { get; set; }
        public virtual Tcc Tcc { get; set; }
    }
}
