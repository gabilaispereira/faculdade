﻿using Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Entities
{
    [Table("AlunoTcc")]
    public class AlunoTcc : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [ForeignKey("Aluno")]
        public string CPFAluno { get; set; }
        public virtual Aluno Aluno { get; set; }

        [ForeignKey("Tcc")]
        public int CodigoTcc { get; set; }
        public virtual Tcc Tcc { get; set; }
    }
}
