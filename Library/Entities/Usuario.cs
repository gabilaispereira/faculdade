﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities.Common
{
    [Table("Usuario")]
    public class Usuario : IdentityUser<Guid>
    {
        #region -> Propriedades
        /// <summary>
        ///  Nome
        /// </summary>
        [MaxLength(50)]
        public string Nome { get; set; }

        /*[ForeignKey("Empresa")]
        public Guid? EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }
*/
        #endregion
    }

}