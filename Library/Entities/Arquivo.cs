﻿using Core.Entities.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("Arquivo")]
    public class Arquivo : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }

        [MaxLength(255)]
        public string NomeArquivo { get; set; }

        public string Url { get; set; }

    }
}
