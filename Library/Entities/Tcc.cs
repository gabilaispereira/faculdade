﻿using Core.Entities.Common;
using Microsoft.CodeAnalysis.Options;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Entities
{
    [Table("Tcc")]
    public class Tcc : CommonEntity
    {
        [Key]
        public int Codigo { get; set; }
        public string Titulo { get; set; }
        public int QuantidadePalavras { get; set; }
        [ForeignKey("Arquivo")]
        public int CodigoArquivo { get; set; }
        public virtual Arquivo Arquivo { get; set; }
        [ForeignKey("Professor")]
        public string? CpfProfessor { get; set; }
        public virtual Professor Professor { get; set; }

        [NotMapped]
        public string NomeArquivo { get; set; }
        [NotMapped]
        public string Url { get; set; }
    }
}
