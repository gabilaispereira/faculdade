﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class RetornoViewModel
    {
        public RetornoViewModel(string mensagem = null, bool sucesso = false, int codigoRetorno = 0)
        {
            Sucesso = sucesso;
            Mensagem = mensagem;
            CodigoRetorno = codigoRetorno;
        }

        public bool Sucesso;
        public int CodigoRetorno;
        public string Mensagem;
        public object Erros;
    }
}
