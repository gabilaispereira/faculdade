﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel(string nome, string icone, string link)
        {
            Nome = nome;
            Icone = icone;
            Link = link;
        }
        public string Nome { get; set; }
        public string Icone { get; set; }
        public string Link { get; set; }
        public bool Selecionado { get; set; }
    }
}
