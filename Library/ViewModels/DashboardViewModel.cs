﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class DashboardViewModel
    {
        public int QtdeProfessor { get; set; }
        public int QtdeProfessorBanca { get; set; }
        public int QtdeAluno { get; set; }
        public int QtdeAlunoAtivo { get; set; }
        public int QtdeAlunoMatriculado { get; set; }
        public int QtdeDisciplina { get; set; }
        public int QtdeDisciplinaAtiva { get; set; }
        public int QtdeTcc { get; set; }
        public int QtdeBanca { get; set; }
    }
}
