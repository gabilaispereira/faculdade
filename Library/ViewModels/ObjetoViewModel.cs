﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class ObjetoViewModel
    {
        public string Colunas { get; set; }
        public string ColunaValor { get { return $"@{Colunas}"; } }
        public string ColunaMaisValor { get { return $"{Colunas} = @{Colunas}"; } }

    }
}
