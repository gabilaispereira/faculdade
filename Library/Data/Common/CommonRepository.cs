﻿using Dapper;
using Core.Entities.Common;
using Core.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations;
using KeyAttribute = System.ComponentModel.DataAnnotations.KeyAttribute;
using Library.ViewModels;

namespace Core.Data.Common
{
    public class CommonRepository<T> : ICommonRepository<T> where T : CommonEntity
    {
        protected readonly Context.Context _dbContext;

        private readonly IConfiguration _configuration;

        public CommonRepository(Context.Context dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        protected string TccConnection()
        {
            return _configuration.GetConnectionString("TccConnection");
        }

        private PropertyInfo GetPrimaryKey(Type tipo)
        {
            try
            {
                var propriedades = tipo.GetProperties();

                foreach (var propriedade in propriedades)
                {
                    if (propriedade.GetCustomAttribute(typeof(KeyAttribute)) != null)
                        return propriedade;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        private string GetPrimaryKeyName(Type tipo)
        {
            try
            {
                var primaryKey = GetPrimaryKey(tipo).Name;

                return primaryKey;
            }
            catch
            {
                return null;
            }
        }

        public T GetByKey(string key)
        {
            try
            {
                var primaryKey = GetPrimaryKeyName(typeof(T));

                var sql = $"SELECT * FROM {typeof(T).Name} WHERE {primaryKey} = @KEY";

                return RetornaQuery(sql, new { KEY = key });
            }
            catch

            {
                return null;
            }
        }

        public IEnumerable<T> GetAll()
        {
            using (var dp = new SqlConnection(TccConnection()))
            {
                try
                {
                    var sql = $"SELECT * FROM {typeof(T).Name}";
                    var retorno = dp.Query<T>(sql);

                    return retorno;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public void DeleteByKey(string key)
        {
            using (var dp = new SqlConnection(TccConnection()))
            {
                try
                {
                    var primaryKey = GetPrimaryKeyName(typeof(T));
                    var sql = $"DELETE {typeof(T).Name} WHERE {primaryKey} = @KEY";
                    ExecutaQuery(sql, new { KEY = key });
                }

                catch (Exception e)
                {
                    throw new Exception("Erro ao deletar: " + e.Message);
                }

            }
        }

        public object InsertOrReplace(T entity)
        {
            using (var dp = new SqlConnection(TccConnection()))
            {
                try
                {
                    var primaryKey = GetPrimaryKey(typeof(T));

                    var valor = primaryKey.GetValue(entity);

                    var sql = $"SELECT * FROM {typeof(T).Name} WHERE {primaryKey.Name} = '{valor}'";
                    var retorno = dp.QuerySingleOrDefault<T>(sql);

                    if (retorno == null)
                    {
                        return Update(entity);
                    }
                    else
                    {
                        return Insert(entity);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Erro: " + e.Message);
                }
            }
        }

        public T RetornaQuery(string query, object parametro = null)
        {
            // Inicializa conexão com dapper
            using (var dp = new SqlConnection(TccConnection()))
            {
                try
                {
                    dp.Open();
                    return dp.QueryFirst<T>(query, parametro);
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    dp.Close();
                }
            }
        }

        public object ExecutaQuery(string query, object parametro = null)
        {

            // Inicializa conexão com dapper  
            using (var dp = new SqlConnection(TccConnection()))
            {
                try
                {
                    dp.Open();
                    dp.Execute(query, parametro);

                    return new
                    {
                        Sucesso = true
                    };
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                finally
                {
                    dp.Close();
                }
            }
        }

        public RetornoViewModel Update(T entity)
        {
            try
            {
                //Recebe os campos e valores da entidade
                var campos = ConstroiObjeto(entity);
                var primaryKey = GetPrimaryKey(typeof(T));
                var valor = primaryKey.GetValue(entity);
                //Monta query com todos as colunas e valores
                var query = $"UPDATE {typeof(T).Name} SET {string.Join(",", campos.Where(c => c.Colunas != "Id").Select(c => c.ColunaMaisValor))} WHERE {primaryKey.Name} = '{valor}' ";
                //Executa a query de cima
                ExecutaQuery(query, entity);

                return new RetornoViewModel()
                {
                    Sucesso = true
                };
            }
            catch (Exception e)
            {
                return new RetornoViewModel()
                {
                    Sucesso = true,
                    Mensagem = "Erro, " + e.Message
                };
            }
        }

        /// <summary>
        /// Metodo individual de Insert
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public RetornoViewModel Insert(T entity)
        {
            try
            {
                //Recebe os campos e valores da entidade
                var campos = ConstroiObjeto(entity);
                //Monta query com todos as colunas e valores
                var query = $"INSERT INTO {typeof(T).Name} ({string.Join(",", campos.Select(c => c.Colunas))}) VALUES ({string.Join(",", campos.Select(c => c.ColunaValor))}) ";
                //Executa a query de cima
                ExecutaQuery(query, entity);

                return new RetornoViewModel()
                {
                    Sucesso = true
                };
            }
            catch (Exception e)
            {
                return new RetornoViewModel()
                {
                    Sucesso = false,
                    Mensagem = "Erro, " + e.Message
                };
            }
        }

        private IEnumerable<ObjetoViewModel> ConstroiObjeto(T entity)
        {
            //Guarda Objeto recebido
            var objeto = (T)entity;
            //Monta objeto com os campos (NOME/VALOR) - Ignorando campos NotMapped & Virtuais
            return typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(p => !p.GetAccessors().Any(a => a.IsVirtual) && !Attribute.IsDefined(p, typeof(NotMappedAttribute)))
                .Select(c => new ObjetoViewModel { Colunas = c.Name }).ToList().Where(c => c.Colunas != "Codigo");
        }
    }
}
