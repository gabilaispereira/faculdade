﻿using Core.Entities.Common;
using Library.ViewModels;

namespace Core.Data.Common
{
    public interface ICommonRepository<T> where T : CommonEntity
    {
        T GetByKey(string key);

        void DeleteByKey(string key);

        object InsertOrReplace(T entity);

        T RetornaQuery(string query, object parametro = null);

        object ExecutaQuery(string query, object parametro = null);

        IEnumerable<T> GetAll();
        RetornoViewModel Update(T entity);
        RetornoViewModel Insert(T entity);
    }
}
