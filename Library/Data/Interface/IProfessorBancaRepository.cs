﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface IProfessorBancaRepository : ICommonRepository<ProfessorBanca>
    {
    }
}
