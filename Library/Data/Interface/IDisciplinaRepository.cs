﻿using Core.Data.Common;
using Library.Entities;

namespace Library.Data.Interface
{
    public interface IDisciplinaRepository : ICommonRepository<Disciplina>
    {
    }
}