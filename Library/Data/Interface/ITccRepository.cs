﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface ITccRepository : ICommonRepository<Tcc>
    {
    }
}
