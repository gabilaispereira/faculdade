﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface IAlunoRepository : ICommonRepository<Aluno>
    {
    }
}
