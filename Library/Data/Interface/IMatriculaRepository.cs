﻿using Core.Data.Common;
using Library.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Data.Interface
{
    public interface IMatriculaRepository : ICommonRepository<Matricula>
    {
    }
}
