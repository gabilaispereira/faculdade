﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Data.Common;
using Library.Entities;

namespace Library.Data.Interface
{
    public interface IProfessorRepository : ICommonRepository<Professor>
    {
    }
}
