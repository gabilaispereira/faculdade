﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface IArquivoRepository : ICommonRepository<Arquivo>
    {
    }
}
