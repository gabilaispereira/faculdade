﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface IBancaRepository : ICommonRepository<Banca>
    {
    }
}
