﻿using Library.Entities;
using Library.ViewModels;

namespace Core.Data.Common
{
    public interface IHomeRepository
    {
        DashboardViewModel GetDashboard();
    }
}
