﻿using Library.Entities;

namespace Core.Data.Common
{
    public interface IProfessorTccRepository : ICommonRepository<ProfessorTcc>
    {
    }
}
