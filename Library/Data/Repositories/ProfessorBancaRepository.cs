﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class ProfessorBancaRepository : CommonRepository<ProfessorBanca>, IProfessorBancaRepository
    {
        public ProfessorBancaRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
