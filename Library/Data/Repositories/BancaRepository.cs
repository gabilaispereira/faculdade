﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class BancaRepository : CommonRepository<Banca>, IBancaRepository
    {
        public BancaRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
