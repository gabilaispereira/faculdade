﻿using Microsoft.Extensions.Configuration;
using Library.Entities;
using Core.Context;
using Library.Data.Interface;
using Core.Data.Common;

namespace Library.Data.Repositories
{
    public class DisciplinaRepository : CommonRepository<Disciplina>, IDisciplinaRepository
    {
        public DisciplinaRepository(Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
