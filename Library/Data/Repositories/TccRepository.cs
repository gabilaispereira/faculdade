﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class TccRepository : CommonRepository<Tcc>, ITccRepository
    {
        public TccRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
