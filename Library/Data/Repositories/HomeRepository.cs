﻿using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Dapper;
using Library.ViewModels;

namespace Core.Data.Common
{
    public class HomeRepository : IHomeRepository
    {
        protected readonly Context.Context _dbContext;
        private readonly IConfiguration _configuration;

        public HomeRepository(Context.Context dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }
        protected string TccConnection()
        {
            return _configuration.GetConnectionString("TccConnection");
        }
        
        public DashboardViewModel GetDashboard()
        {
            try
            {
                using (var dp = new SqlConnection(TccConnection()))
                {
                    try
                    {
                        var sql =
                            " SELECT                                                                                                           " +
                            " (SELECT COUNT(*) FROM Professor) AS QtdeProfessor,                                                               " +
                            " (SELECT COUNT(*) FROM Professor P                                                                                " +
                            " JOIN PROFESSORBANCA PB ON PB.CPFPROFESSOR = P.Cpf                                                                " +
                            " ) AS QtdeProfessorBanca,                                                                                         " +
                            " (SELECT COUNT(*) FROM Aluno) AS QtdeAluno,                                                                       " +
                            " (SELECT COUNT(*) FROM Aluno WHERE DataConclusao <= GETDATE() OR DataConclusao IS NULL) AS QtdeAlunoAtivo,        " +
                            " (SELECT COUNT(*) FROM Aluno A                                                                                    " +
                            " JOIN Matricula M ON M.CPFAluno = A.Cpf) AS QtdeAlunoMatriculado,                                                 " +
                            " (SELECT COUNT(*) FROM Disciplina) AS QtdeDisciplina,                                                             " +
                            " (SELECT COUNT(*) FROM Disciplina D                                                                               " +
                            " JOIN Matricula M ON M.CodigoDisciplina = D.Codigo                                                                " +
                            " ) AS QtdeDisciplinaAtiva,                                                                                        " +
                            " (SELECT COUNT(*) FROM Tcc) AS QtdeTcc,                                                                           " +
                            " (SELECT COUNT(*) FROM Banca) AS QtdeBanca                                                                        " ;

                        var retorno = dp.QueryFirst<DashboardViewModel>(sql);

                        return retorno;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
