﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class ArquivoRepository : CommonRepository<Arquivo>, IArquivoRepository
    {
        public ArquivoRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
