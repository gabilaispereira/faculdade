﻿using Core.Context;
using Core.Data.Common;
using Library.Data.Interface;
using Library.Entities;
using Microsoft.Extensions.Configuration;

namespace Library.Data.Repositories
{
    public class AlunoTccRepository : CommonRepository<AlunoTcc>, IAlunoTccRepository
    {
        public AlunoTccRepository(Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
