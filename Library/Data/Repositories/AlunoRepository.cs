﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class AlunoRepository : CommonRepository<Aluno>, IAlunoRepository
    {
        public AlunoRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
