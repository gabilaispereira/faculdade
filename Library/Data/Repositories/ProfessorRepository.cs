﻿using Microsoft.Extensions.Configuration;
using Library.Entities;
using Core.Context;
using Library.Data.Interface;
using Core.Data.Common;

namespace Library.Data.Repositories
{
    public class ProfessorRepository : CommonRepository<Professor>, IProfessorRepository
    {

        public ProfessorRepository(Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
