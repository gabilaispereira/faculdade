﻿using Microsoft.Extensions.Configuration;
using Library.Entities;

namespace Core.Data.Common
{
    public class ProfessorTccRepository : CommonRepository<ProfessorTcc>, IProfessorTccRepository
    {
        public ProfessorTccRepository(Context.Context dbContext, IConfiguration configuration) : base(dbContext, configuration)
        { }
    }
}
